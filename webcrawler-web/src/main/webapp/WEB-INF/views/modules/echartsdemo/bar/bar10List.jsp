<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/bar10">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/bar10" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
                var option = {
                	    tooltip : {
                	        trigger: 'axis',
                	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                	        }
                	    },
                	    legend: {
                	        data:['利润', '支出', '收入']
                	    },
                	    toolbox: {
                	        show : true,
                	        feature : {
                	            mark : {show: true},
                	            dataView : {show: true, readOnly: false},
                	            magicType : {show: true, type: ['line', 'bar']},
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    calculable : true,
                	    xAxis : [
                	        {
                	            type : 'value'
                	        }
                	    ],
                	    yAxis : [
                	        {
                	            type : 'category',
                	            axisTick : {show: false},
                	            data : ['周一','周二','周三','周四','周五','周六','周日']
                	        }
                	    ],
                	    series : [
                	        {
                	            name:'利润',
                	            type:'bar',
                	            itemStyle : { normal: {label : {show: true, position: 'inside'}}},
                	            data:[200, 170, 240, 244, 200, 220, 210]
                	        },
                	        {
                	            name:'收入',
                	            type:'bar',
                	            stack: '总量',
                	            barWidth : 5,
                	            itemStyle: {normal: {
                	                label : {show: true}
                	            }},
                	            data:[320, 302, 341, 374, 390, 450, 420]
                	        },
                	        {
                	            name:'支出',
                	            type:'bar',
                	            stack: '总量',
                	            itemStyle: {normal: {
                	                label : {show: true, position: 'left'}
                	            }},
                	            data:[-120, -132, -101, -134, -190, -230, -210]
                	        }
                	    ]
                	};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>