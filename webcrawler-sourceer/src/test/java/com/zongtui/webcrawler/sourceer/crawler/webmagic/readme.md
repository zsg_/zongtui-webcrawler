OschinaBlogPageProcessor
这里通过page.addTargetRequests()方法来增加要抓取的URL，并通过page.putField()来保存抽取结果。
page.getHtml().xpath()则是按照某个规则对结果进行抽取，这里抽取支持链式调用。调用结束后，
toString()表示转化为单个String，all()则转化为一个String列表。

Spider是爬虫的入口类。Pipeline是结果输出和持久化的接口，这里ConsolePipeline表示结果输出到控制台。

执行这个main方法，即可在控制台看到抓取结果。webmagic默认有3秒抓取间隔，请耐心等待。
你可以通过site.setSleepTime(int)修改这个值。site还有一些修改抓取属性的方法。